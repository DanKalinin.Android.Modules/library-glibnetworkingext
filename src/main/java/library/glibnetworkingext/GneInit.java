package library.glibnetworkingext;

import android.content.Context;
import androidx.startup.Initializer;
import java.util.ArrayList;
import java.util.List;
import library.glibext.GeInit;
import library.gnutlsext.TlsInit;
import library.java.lang.LjlObject;

public class GneInit extends LjlObject implements Initializer<Void> {
    @Override
    public Void create(Context context) {
        System.loadLibrary("library-glibnetworkingext");
        return null;
    }

    @Override
    public List<Class<? extends Initializer<?>>> dependencies() {
        ArrayList<Class<? extends Initializer<?>>> ret = new ArrayList<>();
        ret.add(GeInit.class);
        ret.add(TlsInit.class);
        return ret;
    }
}
