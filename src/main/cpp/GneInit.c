//
// Created by Dan on 15.10.2021.
//

#include "GneInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    gne_init();

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {

}
